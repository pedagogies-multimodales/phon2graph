# -*- encoding:utf8 -*- 

######### phon2graph.py #########
#
# Version Python 3.7.1
#
# Prend en arguments :
#	- un mot
#	- sa transcription
#	- un dictionnaire qui donne chaque graphie pour chaque phonème
#	- un dictionnaire qui donne la classe de chaque phonème (classe css pour afficher en couleurs)
#
#	Mode verbose : ajouter verb=True pour afficher le détail dans la console.
#
# Sylvain Coulange (2021)
# cf. README pour explications détaillées


import re


# DECOUPAGE PHONOGRAPHEMIQUE
def decoupage(mot,trans,phon2graph,phon2class,verb=False):

    def printlog(*txt):
        if verb:
            outp = ''
            for i in txt:
                outp += str(i)+' '
            print(outp)

    phonographie = []

    # Listes des exceptionss
    scphListe = ['s','c','p','t', 'w', 'g', 'r'] # avec -h donnent 1 phonème (reste problème avec désherber...) FidelDo : "ah","ach","euh","ah","oh","wh","ph","th","ch","sth","ch","sh","sch","cch","ch","gh","rrh","rh"
    xListe = ['tʃ','dz','dʒ','ks','ɡz','ɥi','ij','ts','ɑ̃','ɔ̃','ɛ̃','œ̃'] # peuvent donner 1 graphème (cas de 'wa' à part)
    
    # Prétraitements de la transcription et du mot orthographié
    trans = trans.replace('.','')
    trans = trans.replace('‿','') # il faudra traiter ce caractère prochainement.
    trans = trans.replace(' ','')
    trans = trans.replace('(','')
    trans = trans.replace(')','')
    trans = trans.replace('͡','')
    trans = trans.replace('*','')
    mot = mot.lower()
    #mot = mot.replace('-','')
    #mot = mot.replace(' ','')

    # Déclarations préalables des mémoires 1 et 2
    graphWinners_precedent2 = []
    graphWinners_precedent = []
    mot_save2 = ''
    mot_save = ''
    phon_save2= ''
    phon_save = ''
    trans_save2 = ''
    trans_save = ''

    echec = False # permet de renvoyer le mot sans trancription si impossible à coloriser (ex CM2)
    motOrigin = mot # permet de renvoyer le mot d'origine

    # Association phonème→graphème
    # Boucle sur la chaîne de phonèmes à partir du dernier
    iteration = 0
    while trans != '' and iteration<=50:
        iteration+=1
        printlog('##################################')
        printlog('Départ : "',mot,'", [',trans,']')
        
        if len(mot) == 0:
            echec = True
            printlog('ÉCHEC: plus de lettre à aligner!')
            break
        if len(trans) == 0:
            echec = True
            printlog('ÉCHEC: plus de phonème à aligner!')
            break

        if not echec:
            if mot[-1] == ' ' or mot[-1] == '-':
                printlog('Dernier caractère de mot est une espace ou un tiret: on ajoute ce caractère en phon_neutre')
                phonographie.append(("phon_neutre",mot[-1]))
                mot = mot[:-1]
            elif trans[-1] == '‿':
                # bien-être /bjɛ̃n‿ɛtʁ/ → on aligne /n‿/ à '‿' (à ajouter)
                # peut-être /pø.t‿ɛtʁ/ → on aligne /t‿/ à '‿' (à ajouter)
                # !! mal-être /mal‿ɛtʁ/ → on aligne /l‿/ à '‿' (à ajouter) MAIS on ne supprime par /l/ !!
                printlog('Dernier caractère de transcription est "‿": on ajoute ce caractère à mot avec aligné au phonème qui suit.')
                phonographie.append((phon2class[trans[-2]],'‿'))
                trans = trans[:-2] # on supprime les deux derniers car de la transcription x‿
            else:
                # Traitement de wa
                if trans[-2:]=='wa':
                    print("traitement de wa !")
                    if re.match(r'^.*(o|oi|oî|oe|oê|oie|oix|oid|oig|hoi|oye|ois|oîs|oît|oit|oits|oies|oient|oids|oigt|oigts|oyes)$',mot): # on considère le phonème /wa/ SI le mot termine par oi,oî,oe,oê,o,oie,oix,oid,oig,hoi,oye,ois,oîs,oît,oit,oits,oies,oient,oids,oigt,oigts,oyes
                        phon = trans[-2:]
                        print("Détection de ois !")
                    else:
                        phon = trans[-1:] # sinon on considère /w/ puis /a/ par défaut
                
                elif trans[-2:]=='wɑ':
                    if re.match(r'^.*(oix|ois|oids)$',mot): # cf liste wɑ de PronSci
                        phon = trans[-2:]
                    else:
                        phon = trans[-1:] # sinon on considère /w/ puis /ɑ/ par défaut
                
                # Traitement des caractères phonèmes à prendre par deux
                elif trans[-2:] in xListe:
                    # EXCEPTIONS
                    # Traitement de wɛ̃
                    if trans[-3:]=='wɛ̃':
                        if re.match(r'^.*(oin|oint|oins|oints|oing|oings)$',mot): # on considère le phonème /wɛ̃/ SI le mot termine par oin,oint,oins,oints,oing,oings
                            phon = trans[-3:]
                        else:
                            phon = trans[-2:]
                    elif trans[-2:]=='ks' and re.match(r'^.*xc$',mot): # permettre 'xc' comme [ks]
                        phon = trans[-1]
                    elif trans[-2:]=='ks' and not re.match(r'^.*x$',mot): # éviter de traiter 'ct' (fonction) comme [ks]
                        phon = trans[-1]
                    elif trans[-2:]=='dʒ' and re.match(r'^.*dj.*$',mot) or re.match(r'^.*dg.*$',mot): # si 'dj' (adjectif) alors 'j' = [ʒ] et pas [dʒ] pareil pour badge
                        phon = trans[-1]
                    elif trans[-2:]=='ɥi' and not mot[-1]=='u': # cas de tuile, fuite, aujourd'hui, appuis → aligner a pp u is
                        phon = 'i'
                    elif trans[-2:]=='ij' and re.match(r'^.*ll[ents]*$',mot): # permettre ill comme [ij] #gri lle
                        phon = trans[-1]
                    elif trans[-2:]=='ts' and re.match(r'^.*ts$',mot): # on traite les formes orthographiques "ts" comme /t/+/s/ par défaut. Mais on permet /ts/ pour "tu" Québec par exemple
                        phon = trans[-1]
                    else:
                        phon = trans[-2:]
                    
                # Sinon le phonème correspond au dernier caractère de la transcription
                else:
                    phon = trans[-1]

                printlog("Cherche : [",phon,"]")

                # Récupération des graphies possibles dans le Fidel étant donné le mot
                graphWinners = [] # liste des possibilités de graphie (dans le cas où ce n'est pas la graphie la plus longue qu'il faut choisir)
                if phon in phon2graph.keys():
                    # Boucle sur la liste de graphies possible pour le phonème phon
                    for graph in phon2graph[phon]:
                        # On cherche cette graphie dans la fin du mot
                        s = re.match(r'^.*'+graph+'$',mot)
                        if s and graph[0]=='h' and len(mot)>len(graph) and mot[-(len(graph)+1)] in scphListe:
                            # si la graphie commence par h, et la lettre qui précède dans le mot fait partie de scphListe, alors on ne prend pas cette graphie (le h fait partie du phonème suivant)
                            # pour les graphies commençant par h, si le mot est plus long que la graphie on aligne h avec le phonème suivant (télé ph one, th éâtre)
                            printlog('!! trouvé',graph,'; on laisse',graphWinners)
                        # elif s and graph=="ign" and phon=="ɲ":
                        #     # SI ON VEUT ALIGNER O-IGN-ON :
                        #     # Cas de oignon, oignonade, oignonet, oignonière, oignonette
                        #     # dans ce cas on garde la graphie "ign", sinon on prend pas (beignet, poignée...)
                        #     if mot=="oign":
                        #         graphWinners.append(graph)
                        #         printlog('trouvé',graph)
                        elif s :
                            graphWinners.append(graph)
                            printlog('trouvé',graph)
                else:
                    printlog('Phonème',phon,'non trouvé dans le Fidel !')
                
                graphWinners = sorted(graphWinners, key=len) # on réordonne les graphies par taille
                printlog(graphWinners)

                if len(graphWinners)>0:
                    printlog('OK len(graphWinners)>0',graphWinners, 'ajout de :', graphWinners[-1])
                    phonographie.append((phon2class[phon],graphWinners[-1]))
                    
                    # MISES EN MÉMOIRE (1→2, courant→1)
                    graphWinners_precedent2 = graphWinners_precedent # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog('update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog('update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save
                    printlog('update phon_save2 :',phon_save2)
                    phon_save = phon # on garde en mémoire le phonème aussi
                    printlog('update phon_save :',phon_save)
                    
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription
                    
                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(graphWinners[-1])]
                    trans = trans[:-len(phon)] # et on enlève le dernier phonème pour passer au suivant
                    printlog('trans :',trans_save,'→',trans)

                    printlog('phonographie :',phonographie)

                elif len(graphWinners_precedent) > 0:
                    # Cas où on a pas trouvé de graphie pour le phonème courant, parce que la graphie précédente n'était pas la bonne (elle est trop longue)
                    remplacant = graphWinners_precedent[-1]
                    printlog("Aucune graphie touvée ! → On revient sur le dernier phonème et on prend sa graphie suivante :", remplacant)
                    phonographie = phonographie[:-1] # on supprime la dernière graphie (qui n'était pas la bonne)
                    phonographie.append((phon2class[phon_save],remplacant)) # on y met à la place le graphie suivante avec son phonème
                    
                    # CORRECTION DES VARIABLES
                    trans = trans_save # on remet la transcription du coup d'avant (avant de la tronquer plus bas)
                    mot = mot_save # pareil avec le mot

                    # MISES EN MÉMOIRE (1→2, courant→1)
                    #graphWinners_precedent2 = graphWinners_precedent[:-1] # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog('update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners_precedent[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog('update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save2
                    printlog('update phon_save2 :',phon_save2)
                    phon_save = phon_save # on garde en mémoire le phonème aussi
                    printlog('update phon_save :',phon_save)
                    
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription
                    
                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(remplacant)]
                    trans = trans[:-len(phon_save)] # et on enlève le dernier phonème pour passer au suivant (attention phon_save est passé à save2)
                    printlog('trans :',trans_save,'→',trans)

                    printlog('phonographie :',phonographie)

                elif len(graphWinners_precedent2) > 0:
                    # Si pas d'autres graphies possibles pour le phonème précédent, on va chercher la graphie suivante du phonème d'avant
                    remplacant = graphWinners_precedent2[-1]
                    phonConcerne = phon_save2
                    printlog("Aucune graphie touvée ! Ni pour le phonème d'avant ! → on va chercher la graphie suivante du phonème encore avant :",remplacant)
                    phonographie = phonographie[:-2] # on supprime les 2 dernières graphies (qui n'étaient pas la bonnes)
                    phonographie.append((phon2class[phonConcerne],remplacant)) # on y met à la place le graphie suivante avec son phonème
                    
                    # CORRECTION DES VARIABLES
                    trans = trans_save2 # on remet la transcription du coup d'avant (avant de la tronquer plus bas)
                    mot = mot_save2 # pareil avec le mot

                    # MISES EN MÉMOIRE (1→2, courant→1)
                    #graphWinners_precedent2 = graphWinners_precedent2[:-1] # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog('update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners_precedent2[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog('update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save
                    printlog('update phon_save2 :',phon_save2)
                    phon_save = phon # on garde en mémoire le phonème aussi
                    printlog('update phon_save :',phon_save)
                
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription

                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(remplacant)]
                    trans = trans[:-len(phonConcerne)] # et on enlève le dernier phonème pour passer au suivant (attention phon_save2 a été écrasé donc il faut le garder en mémoire temporaire (phonConcerne))
                    

                    printlog('trans :',trans_save,'→',trans)

                    printlog('phonographie :',phonographie)
                else:
                    printlog('AUCUNE GRAPHIE TROUVÉE POUR CE MOT !')
                    echec = True
                    break
    ## FIN DE LA BOUCLE SUR TRANSCRIPTION

    ## FINALISATION ET ENVOI DU RÉSULTAT
    if echec:
        printlog("Mot impossible à coloriser. Renvoie du mot sans couleur.")
        phonographie = [('phon_echec',motOrigin)]
        printlog(("Phonographie :",phonographie))
    else:
        # Si le mot a bien été colorisé jusqu'à la fin
        # On remet tout dans l'ordre et on envoie
        phonographie.reverse()
        printlog("FINI !",phonographie)
        
    printlog("###########")
    printlog("###########")
    return phonographie # revoie donc un liste de (phonème(s), graphème(s))