def pinyin2phon(pintone, pinyin2api, api2class):
    ## Prend en argument une chaîne type 'ni2' ou 'hao3'
    ## Renvoie tuple (car, api, phonlist, ton)

    ##### LISTES EXCEPTIONS PINYIN #####
    xjq = ['x','j','q'] # suivis de -u → /-y/
    uun = ['u','un'] # précédés de xjq font /y/
    ilist = ['z','c','s','zh','ch','sh','r'] # -i is a buzzed continuation of the consonant following z-, c-, s-, zh-, ch-, sh- or r-.
    avoidlist = ['kw', 'wa', 'xw', 'ən', 'lj', 'nj', 'mj', 'jʊ', ] # éviter de les considérer comme un seul phonème


    car = pintone[:-1]
    ton = pintone[-1]

    if car in pinyin2api.keys():
        # Toute la graphie est dans pinyin2api # yi
        api = pinyin2api[car]
    else:
        if car[:2] in pinyin2api.keys():
            # 2 premiers caractères sont dans pinyin2api # zh eng
            api = pinyin2api[car[:2]] # api = zh
            if car[2:] in pinyin2api.keys(): # eng
                if car[:2] in ilist and car[2:] == 'i': # ! zh i / sh i
                    api += pinyin2api['_' + car[2:]]
                else:
                    api += pinyin2api[car[2:]] # zh eng
            else:
                api = ""
                print(car[:2], "trouvé ;", car[2:], "non trouvé!")
        
        elif car[:1] in pinyin2api.keys():
            # le premier caractère est dans pinyin2api # h ao
            api = pinyin2api[car[:1]]
            if car[1:] in pinyin2api.keys(): # ao
                if car[0] in xjq and car[1:] in uun: # ! qu / ju / xu
                    api += pinyin2api['_' + car[1:]]
                elif car[0] in ilist and car[1:] == 'i': # ! ri / ci / zi ...
                    api += pinyin2api['_' + car[1:]]
                else:
                    api += pinyin2api[car[1:]]
            else:
                api = ""
                print(car[:1], "trouvé ;", car[1:], "non trouvé!")
        else:
            print(car[:1], "non trouvé!")

    ## Conversion api2class
    phonlist = []
    
    phon = ''
    restapi = api
    while len(restapi) > 0:
        i=0
        trouve = True
        while trouve and i<=len(restapi):
            i+=1
            if restapi[:i] in api2class.keys() and restapi[:i] not in avoidlist:
                continue
                print(i, restapi[:i])
            else:
                i = i-1
                trouve = False
        print("trouvé",restapi[:i])
        phonlist.append(api2class[restapi[:i]])

        restapi = restapi[i:]
    
    return (car, api, phonlist, ton)


def pinyin2phon2(pintone, pinyin2api, api2class, pinyin2zhuyin, verb=False):
    ## Prend en argument une chaîne type 'ni2' ou 'hao3'
    ## Renvoie tuple (car, api, phonlist, ton, {otherTrans})

    def printlog(*txt):
        if verb:
            outp = ''
            for i in txt:
                outp += str(i)+' '
            print(outp)

    phonographies = [] # contiendra la liste des phonographies possibles ex. :
    exemple_phonographies = [
        {
            "api": "tɕʰje",
            "phonographiePinyin": [
                ["q","phon_t_hs_retr-h"],
                ["j","phon_j"],
                ["e","phon_e"]
            ],
            "phonographieZhuyin": [
                ["ㄑ","phon_t_hs_retr-h"],
                ["ㄧ","phon_j"],
                ["ㄝ","phon_e"]
            ],
            "phonographieWade": [
                ["chʻ","phon_t_hs_retr-h"],
                ["i","phon_j"],
                ["eh","phon_e"]
            ]
        },
        {
            "api": "tɕʰjɛ",
            "phonographiePinyin": [
                ["q","phon_t_hs_retr-h"],
                ["j","phon_j"],
                ["e","phon_ɛ"]
            ],
            "phonographieZhuyin": [
                ["ㄑ","phon_t_hs_retr-h"],
                ["ㄧ","phon_j"],
                ["ㄝ","phon_ɛ"]
            ],
            "phonographieWade": [
                ["chʻ","phon_t_hs_retr-h"],
                ["i","phon_j"],
                ["eh","phon_ɛ"]
            ]
        }
    ]

    ##### LISTES EXCEPTIONS PINYIN #####
    xjq = ['x','j','q'] # suivis de -u → "y"
    bpmf = ['b','p','m','f'] # -eng → "oŋ", "ɤŋ"
    uun = ['u','un'] # précédés de xjq font "y"
    ilist = ['z','c','s','zh','ch','sh','r'] # -i is a buzzed continuation of the consonant following z-, c-, s-, zh-, ch-, sh- or r-.
    avoidlist = ['kw', 'wa', 'xw', 'ən', 'lj', 'nj', 'mj', 'jʊ', ] # éviter de les considérer comme un seul phonème

    # "_er":["a˞", "ɤ˞"], *** si 二,
	# "_eng":["oŋ", "ɤŋ"], *** seulement avec les initiales <b> <p> <m> <f> "],
	# "_i":["ɨ"], *** seulement avec les initiales <zh> <ch> <sh> <s>"],
	# "_u":["y"], *** seulement avec les initiales <j> <q> <x>"],
	# "_uan":["ɥɛn", "ɥɛn"], *** seulement avec les initiales <j> <q> <x>"],
	# "_un":["ɥyn", "yn"], *** seulement avec les initiales <j> <q> <x>"],


    thisPinyin = pintone[:-1]
    ton = pintone[-1]

    if thisPinyin in pinyin2zhuyin.keys(): 
        thisZhuyin = pinyin2zhuyin[thisPinyin]["ZhuYin"]
        printlog(thisPinyin,thisZhuyin)
    else:
        thisZhuyin = ""
        printlog(thisPinyin, "n'a pas été trouvé dans pinyin2zhuyin.json !")

    if thisPinyin in pinyin2api.keys():
        # Toute la graphie est dans pinyin2api # yi
        for api in pinyin2api[thisPinyin]:
            printlog("Trouvé dans pinyin2api:",api)
    else:
        if thisPinyin[:2] in pinyin2api.keys():
            # 2 premiers caractères sont dans pinyin2api # zh-eng
            api = pinyin2api[thisPinyin[:2]] # api = zh
            if thisPinyin[2:] in pinyin2api.keys(): # eng
                if thisPinyin[:2] in ilist and thisPinyin[2:] == 'i': # ! zh i / sh i
                    ############
                    ############ ICI
                    
                    api += pinyin2api['_' + thisPinyin[2:]]
                else:
                    api += pinyin2api[thisPinyin[2:]] # zh eng
            else:
                api = ""
                printlog(thisPinyin[:2], "trouvé ;", thisPinyin[2:], "non trouvé!")
        
        elif thisPinyin[:1] in pinyin2api.keys():
            # le premier caractère est dans pinyin2api # h ao
            api = pinyin2api[thisPinyin[:1]]
            if thisPinyin[1:] in pinyin2api.keys(): # ao
                if thisPinyin[0] in xjq and thisPinyin[1:] in uun: # ! qu / ju / xu
                    api += pinyin2api['_' + thisPinyin[1:]]
                elif thisPinyin[0] in ilist and thisPinyin[1:] == 'i': # ! ri / ci / zi ...
                    api += pinyin2api['_' + thisPinyin[1:]]
                else:
                    api += pinyin2api[thisPinyin[1:]]
            else:
                api = ""
                print(thisPinyin[:1], "trouvé ;", thisPinyin[1:], "non trouvé!")
        else:
            print(thisPinyin[:1], "non trouvé!")
    
    phonographie = {
        "api":api,
        "phonographiePinyin": getPhonographiePinyin(api,thisPinyin),
        "phonographieZhuyin": getPhonographieZhuyin(api,thisZhuyin),
        "phonographieWade": []
    }

    

    ## Conversion api2class
    phonlist = []
    
    phon = ''
    restapi = api
    while len(restapi) > 0:
        i=0
        trouve = True
        while trouve and i<=len(restapi):
            i+=1
            if restapi[:i] in api2class.keys() and restapi[:i] not in avoidlist:
                continue
                print(i, restapi[:i])
            else:
                i = i-1
                trouve = False
        printlog("trouvé",restapi[:i])
        phonlist.append(api2class[restapi[:i]])

        restapi = restapi[i:]
    
    return (thisPinyin, api, phonlist, ton)


def getPhonographiePinyin(api, thispinyin):
    return []

def getPhonographieZhuyin(api, thiszhuyin):
    return []