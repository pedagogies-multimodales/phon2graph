# -*- encoding:utf8 -*- 

######### phon2graph.py #########
#
# Version Python 3.7.1
#
# ENGLISH PHON2GRAPH
#
# Prend en arguments :
#	- un mot
#	- sa transcription
#	- un dictionnaire qui donne chaque graphie pour chaque phonème
#	- un dictionnaire qui donne la classe de chaque phonème (classe css pour afficher en couleurs)
#   - un booléen qui active le mode verbose si True
#
#	Mode verbose : passer mode_verbose à True dans la fonction printlog(verb,)
#
# Sylvain Coulange (2020)
# cf. README pour explications détaillées


import re

vowels = ["ɪ","eə","ɒ","əl","əm","ən","ə","æ","ɑː","eɪ","ɔ","e","aɪ","iː","ɔː","əʊ","aʊ","ɔɪ","ɪə","uː","ɜː","ʊə","juː","jə","jʊə","aɪə","ʌ","ʊ","wʌ","wɑː","waɪ","ye","əz","ɪz","jʊ","u","i","ɪɹ","eɹ","ɛ","ɝ","wa","waɹ","jʊɹ","ju","ʊɹ","ɑ","ɑɹ","ɔɹ","oʊ"]

def printlog(verb,*txt):
    if verb:
        outp = ''
        for i in txt:
            outp += str(i)+' '
        print(outp)


# DECOUPAGE PHONOGRAPHEMIQUE
def decoupageEn(mot,trans,phon2graph,phon2class,verb=False):
    phonographie = []
    msg = "" # message d'erreur à envoyer le cas échéant

    # let's get rid of other special characters
    trans = trans.replace('.','')
    trans = trans.replace('‿','')
    trans = trans.replace(' ','')
    trans = trans.replace('(','')
    trans = trans.replace(')','')
    trans = trans.replace('͡','')
    trans = trans.replace('*','')

    trans = trans.replace('n̩','ən')
    trans = trans.replace('m̩','əm')
    trans = trans.replace('l̩','əl')
    trans = trans.replace('ɐ','ʌ')
    
    #mot = mot.replace('-','')
    #mot = mot.replace(' ','')

    # to be sure to treat lower word
    mot = mot.lower()

    echec = False # permet de renvoyer le mot sans trancription si impossible à coloriser (ex CM2)
    motOrigin = mot # permet de renvoyer le mot d'origine
    stress1 = False
    stress2 = False

    # Association phonème→graphème
    # Boucle sur la chaîne de phonèmes à partir du dernier
    while trans != '':
        printlog(verb,'##################################')
        printlog(verb,'Départ : "',mot,'", [',trans,']')
        
        if len(mot) == 0:
            echec = True
            printlog(verb,'ÉCHEC: plus de lettre à aligner!')
            msg = "Still letters to align, but no more phone (/"+trans+"/)"
            break
        if len(trans) == 0:
            echec = True
            printlog(verb,'ÉCHEC: plus de phonème à aligner!')
            msg = "Still phones to align, but no more letter ("+mot+")"
            break

        ### PROSODY SPECIFIC CHARACTERS
        if trans[0] == 'ˈ':
            stress1 = True
            trans = trans[1:]
            printlog(verb,"stress1 detected")
        elif trans[0] == 'ˌ':
            stress2 = True
            trans = trans[1:]
            printlog(verb,"stress2 detected")
        else:
            phon, stress1, stress2, ksgzStress = getPhon(trans,mot,phon2graph,verb,stress1,stress2)

            printlog(verb,"Trying to align: [",phon,"]")

            #### GET POSSIBLE SPELLINGS FOR PHON
            # Get all potential spelling of phon given our word
            graphWinners, echec = getGraphWinners(phon,mot,phon2graph,verb) # list of possible spelling (longer first)
            if echec or len(graphWinners)==0:
                msg = "["+phon+"] not found in the Fidel, or not alignable here"
                break

            ### CHOOSE THE GRAPHWINNER AMONG GRAPHWINNERS
            # check if this spelling won't block next alignment
            errcpt = 0
            err = True
            e = False
            while err and errcpt<10:
                if len(graphWinners)==0:
                    printlog(verb,"no more graphWinners!")
                    err = True
                    msg = "No spelling available for ["+phon+"]"
                    break
                
                printlog(verb, "Check if first graphWinners would allow next alignment...")
                if len(mot[len(graphWinners[0]):])==0 and len(trans[len(phon):])==0:
                    # End of word, no need to predict the future
                    err = False
                else:
                    lenPhon = len(phon)+1 if ksgzStress else len(phon) # if phon = kˈs or ɡˈz, count 3 for len(phon)
                    if len(mot[len(graphWinners[0]):])>0 and len(trans[lenPhon:])>0:
                        # If there is still things to align after this
                        # Let's do a simulation of the next step in the alignment loop
                        futureWord = mot[len(graphWinners[0]):]
                        futureTrans = trans[lenPhon:].replace('ˈ','').replace('ˌ','')
                        futurePhon, ss1, ss2, ksgz = getPhon(futureTrans, futureWord, phon2graph, verb, stress1, stress2) # no need of future stress detection(ss1, ss2), it's just a verification here, neither for ksgzStress
                        if futurePhon not in phon2graph.keys():
                            msg = "No spelling available for ["+futurePhon+"]"
                            printlog("No spelling available for future["+futurePhon+"]")
                            err = False
                        
                        futureGraphWinners, e = getGraphWinners(futurePhon, futureWord, phon2graph, False)
                        printlog(verb, "... '",futureWord,"' /",futureTrans,"/ → phon [",futurePhon,"] => possible spellings:",futureGraphWinners)

                        # EXCEPTION : teacher /tiːtʃə/
                        # here "c" get aligned with /tʃ/ because "he" still can be aligned with /ə/, but "r" remains and nothing is sent back
                        # so we could say that with phon="tʃ", if the first 2 letters of the word are "ch" and next phon is a vowel, then align "ch" to /tʃ/
                        if mot[:2]=="ch" and phon=="tʃ" and futurePhon in vowels:
                            printlog(verb,"!exception of type 'teacher': align 'ch' to /tʃ/")
                            graphWinners = ["ch"]
                            err = False
                        # end exception

                        # EXCEPTION : avoid that "ship" is aligned s hi p ; "which" w hi ch
                        elif mot[1:2]=="h" and futurePhon in vowels:
                            printlog(verb,"!exception of type 'ship', 'which': align 'sh', 'wh' to the consonant")
                            graphWinners = [mot[:2]]
                            err = False
                        # end exception

                        elif e or len(futureGraphWinners)==0:
                            printlog(verb, "Nope, let's remove the first winner", graphWinners[0])
                            errcpt += 1
                            graphWinners = graphWinners[1:]
                        else:
                            err = False
                    else:
                        printlog(verb, "Still things to align, let's avoid the shorter winner", graphWinners[0])
                        errcpt += 1
                        graphWinners = graphWinners[1:]

            if len(graphWinners)==0 or err: 
                echec = True
                break

            graphWinner = graphWinners[0]
            printlog(verb, "OK, '",graphWinner,"' is our guy.")

            ### ALIGNMENT
            if len(graphWinners)>0:
                pros = ''
                if phon in vowels and stress1: 
                    pros = " stress1"
                    stress1 = False
                elif phon in vowels and stress2: 
                    pros = " stress2"
                    stress2 = False
                elif phon in ['ə','ʊ','u','i','ɪ','jʊ','ju','ɚ']:
                    pros = " schwa"
                
                if pros == " schwa":
                    phonographie.append((phon2class['!'+phon] + pros, graphWinner))
                else:
                    phonographie.append((phon2class[phon] + pros, graphWinner))
                printlog(verb, "choose", phonographie[-1])

                # let's get rid of what we just aligned and go next
                lenPhon = len(phon)+1 if ksgzStress else len(phon) # if phon = kˈs or ɡˈz, count 3 for len(phon) [2nd TIME TO BE SURE]
                trans = trans[lenPhon:]
                mot = mot[len(graphWinner):]
            else:
                printlog(verb, "No more possible spellings!")
                msg = "No more possible spellings on "+phon
                echec = True
                break

    ## FIN DE LA BOUCLE SUR TRANSCRIPTION

    ## FINALISATION ET ENVOI DU RÉSULTAT
    if echec or len(phonographie)==0:
        printlog(verb,"Mot impossible à coloriser. Renvoie du mot sans couleur.")
        phonographie = [('phon_echec',motOrigin)]
        printlog(verb,("Phonographie :",phonographie))
    else:
        msg = ""
        # Si le mot a bien été colorisé jusqu'à la fin
        printlog(verb,"FINI !",phonographie)
        
    printlog(verb,"###########")
    printlog(verb,"###########")
    return phonographie, msg # revoie donc un liste de (phonème(s), graphème(s)) et un message d'erreur le cas échéant


# FUNCTION TO SELECT NEXT PHON TO LOOK UP IN THE FIDEL
def getPhon(trans,mot,phon2graph,verb,stress1,stress2):
    phon = ''
    # we take one phoneme after an other, except for these ones:
    trigr = ["juː","jʊə","aɪə","wɑː","waɪ","waɹ","jʊɹ"]
    bigr = ["tʃ","ks","dʒ","nj","eə","əl","əm","ən","sz","tθ","ɡz","kʃ","ɡʒ","ts","ɑː","eɪ","aɪ","iː","ɔː","əʊ","aʊ","ɔɪ","ɪə","uː","ɜː","ʊə","jə","wʌ","əz","ɪz","kw","jʊ","ɪɹ","wa","ju","ʊɹ","ɑɹ","ɔɹ","oʊ"]
    # Brooks suggests also a quadrigramme (aʊwə) but we won't take it into account

    # Case of "excess" /ɛkˈsɛs/ → ks as one phonem but need accent detection
    ksgzStress = False
    if re.match(r'(kˈs|ɡˈz).*',trans):
        stress1 = True
        ksgzStress = True
        printlog(verb,"stress1 detected")
    elif re.match(r'(kˌs|ɡˌz).*',trans):
        stress2 = True
        ksgzStress = True
        printlog(verb,"stress2 detected")
    localtrans = trans.replace('ˈ','').replace('ˌ','') # get rid of the accent character just for this function


    if localtrans[:3] in trigr:
        if re.match(r'^('+'|'.join(phon2graph[localtrans[:3]])+').*$',mot): # consider 3 first phonemes as "one" only if appropriate spelling is found
            print("OK",mot)
            phon = localtrans[:3]
        else:
            phon = localtrans[:2] # else consider only the 2 first phonemes

    if localtrans[:2] in bigr and len(phon)<3:
        if re.match(r'^('+'|'.join(phon2graph[localtrans[:2]])+').*$',mot):
            phon = localtrans[:2]
        else:
            phon = localtrans[0]

    # Else take only the first caracter of transcription as The Phoneme to align
    elif len(phon)<3:
        phon = localtrans[0]
    
    return phon, stress1, stress2, ksgzStress


def getGraphWinners(phon, mot, phon2graph, verb):
    graphWinners = []
    echec = False
    
    if phon in phon2graph.keys():
        for graph in phon2graph[phon]:
            # Can we find this spelling at the begining of our current word ?
            s = re.match(r"^"+graph+".*$",mot)
            if s :
                graphWinners.append(graph) # If yes, we add it to graphWinners
        
        graphWinners = sorted(graphWinners, key=len) # order by length, longer at the end
        printlog(verb,"possible spellings are",graphWinners)
    
    else:
        printlog(verb,'Phoneme',phon,'not found in the Fidel!')
        echec = True
    
    return graphWinners, echec