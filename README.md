# phon2graph

Phon2graph est un algorithme d'alignement graphophonémique du français. Il prend en entrée un mot et sa transcription et aligne chaque phonèmes aux graphèmes du mot. Ainsi le couple "bonjour" /bɔ̃ʒuʁ/ donne l'alignement /b/→b, /ɔ̃/→on, /ʒ/→j, /u/→ou, /ʁ/→r.

Phon2graph a été développé pour les besoins de l'application [WikiColor](http://wikicolor.alem-app.fr/), outil permettant de coloriser un texte en fonction de sa prononciation et selon l'approche Silent Way. Conformément aux panneaux du Silent Way, les suites de phonèmes /wa/ ou /ks/ et /gz/ sont traitées comme des suites bicolores indissociables lorsqu'elles sont orthographiées "oi" ou "x". Le mot "oiseau" est donc aligné /wa/→oi, /z/→s, /o/→eau ; "exemple" est aligné /ɛ/→e, /ɡz/→x, /ɑ̃/→em, /p/→p, /l/→le.

Phon2graph utilise un dictionnaire de graphies (Fidel) qui liste toutes les graphies possibles pour chaque phonème de la langue. Pour les besoins de WikiColor, phon2graph représente également chaque phonème par un code inspiré du [X-SAMPA](https://fr.wikipedia.org/wiki/X-SAMPA), plus facilement manipulable informatiquement. Ces codes sont listés [ici](https://gricad-gitlab.univ-grenoble-alpes.fr/pedagogies-multimodales/phon2graph/-/blob/master/data/class2api.json).


## Fonctionnement de l'algorithme
### Prétraitements des chaînes de caractères en entrée
#### Prétraitement de la transcription
phon2graph ignore tous les caractères de la transcription inutiles à l'alignement, comme les points de segmentation des syllabes, les astérisques ou les parenthèses. Ces caractères sont présents dans le [dictionnaire phonétisé](https://gricad-gitlab.univ-grenoble-alpes.fr/pedagogies-multimodales/wikiphon) utilisé par WikiColor, mais ne sont pas utilisés pour l'alignement et sont donc supprimés temporairement pour permettre l'alignement. Voici la liste complète des caractères supprimés : `.͡* ()‿`

#### Prétraitement du mot
Pour le mot, phon2graph supprime temporairement les espaces et les tirets.

### Alignement
Phon2graph boucle sur la transcription et tente d'aligner chaque phonème, un par un, à la plus longue graphie possible dans le mot, étant donné la liste des graphies suggérées par le Fidel pour le phonème en question. Après chaque alignement, le phonème aligné et la graphie choisie ne sont plus pris en compte pour la recherche suivante d'alignement. Le programme s'arrête quand tous les phonèmes ont été alignés et qu'il ne reste plus de lettres dans le mot. Pour des raisons de simplicité, l'alignement se fait en partant de la fin.

Il arrive bien sûr que la graphie à choisir ne soit pas la plus longue (comme "essentiel" où /s/ peut être aligné à "ssent" ou "sent", mais doit l'être seulement à "t" pour arriver jusqu'à la fin du mot). Si un alignement ne permet pas d'aligner tous les phonèmes à tous les graphèmes, l'algorithme revient en arrière pour prendre une graphie moins longue. Actuellement, phon2graph peut revenir jusqu'à deux phonèmes en arrière si l'alignement ne convient pas. Il ne s'est pas encore révélé nécessaire de remonter plus loin dans les choix de l'algorithme.

Voici les phases d'alignement d'un mot qui ne présente pas de difficulté : "bonjour"
1. "bonjour" /bɔ̃ʒuʁ/ : ʁ → 'r'
2. "bonjou" /bɔ̃ʒu/ : u → 'ou'
3. "bonj" /bɔ̃ʒ/ : ʒ → 'j'
4. "bon" /bɔ̃/ : ɔ̃ → 'on'
5. "b" /b/ : b → 'b'

### Identification du phonème à aligner
Par défaut, chaque caractère de la transcription est considéré comme un phonème à aligner (/a/, /t/, /z/ etc.). Cependant, dans certains cas plusieurs caractères constituent un seul phonème, comme /ks/ ou /tʃ/, ou encore /ɛ̃/ (la nasalisation étant un caractère à elle seule) mais aussi /wa/ ou /wɛ̃/ par exemple). Dans ce cas, une série de règles permettent de traiter la suite de caractère comme un seul phonème.

#### Cas de /wɛ̃/
Si les trois derniers caractères de la transcription sont 'wɛ̃', ils sont traités ensemble comme un seul phonème. L'algorithme tente alors un alignement sur les graphies de /wɛ̃/ (oin,oint,oins,oints,oing,oings,ouin,ouins,ooing,ooings...).

#### Cas des phonèmes bigrammes
Si les deux derniers caractères de la transcription font partie de la liste des phonèmes bigrammes ('ts','tʃ','dz','dʒ','ks','ɡz','wa','ɥi','ij','ɑ̃','ɔ̃','ɛ̃'), ils sont à traiter ensemble comme un seul phonème, _sauf dans les cas suivants :_
- le bigramme est 'ks' et le mot se termine par 'xc' (comme "excellent"), dans ce cas on considère les deux caractères comme deux phonèmes indépendants ('x' est aligné à /k/ et 'c' à /s/);
- le bigramme est 'ks' et le mot *ne se termine pas* par 'x' (comme dans "fonction") /s/ est aligné à 't' et /k/ à 'c';
- le bigramme est 'dʒ' et le mot termine par 'dj' ou 'dg' ("adjectif", "badge"), alors /ʒ/ est aligné à 'j' ou 'g' et /d/ est aligné à 'd';
- le bigramme est 'ɥi' et le mot *ne se termine pas* par 'u', dans ce cas on considère les phonèmes /ɥ/ et /i/ indépendamment (cas de "ajourd'hui, tuile, fuite, appuis" etc.);
- le bigramme est 'ij' et le mot finit par 'lle', 'lles' ou 'llent' (trille, quilles, pillent...), dans ce cas on aligne /j/ à ces graphies-là et /i/ à 'i', pour éviter un "ille" bicolore.

### Identification de la graphie cible
Lorsque le phonème à aligner est identifié, phon2graph récupère la liste de ses graphies dans le Fidel, puis sélectionne toutes les graphies alignables dans le mot. Par exemple dans le mot "passes" /pas/, l'alignement du dernier phonème peut se faire sur 's', 'ses' ou 'sses'. Aligner ici /s/ sur 's' ne permettrait pas de terminer l'alignement, l'aligner sur 'ses' donnerait un alignement incorrect : /p/→p, /a/→as, /s/→ses. Par défaut, phon2graph choisit donc en priorité la graphie la plus longue.

Mais la plus longue graphie ne convient pas toujours. Dans "château", aligner /a/ à "hâ" ne permettrait pas de terminer l'alignement, une règle permet donc d'éviter les graphies commençant par un 'h' si le caractère précédent ce 'h' fait partie des caractères suivants : "c s p t w g r". Dans ces cas précis, la probabilité que le 'h' fasse partie de la graphie suivante est plus grande ("wh","ph","th","ch","sth","ch","sh","sch","cch","ch","gh","rrh","rh"). La majorité des alignements convient, mais il reste quelques problèmes : "désherber" ou "déshydrater", pour lesquels l'alignement plante (/z/→'sh' impossible) ; "Bouddhisme" qui est aligné par défaut "dd hi" (notons ici que si on ajoute "d" à la liste des exceptions, "adhésion" sera aligné "dh é") ; "stakhanoviste" ou "kolkhoze" pour lesquels 'h' appartient à la graphie de /a/ et /ɔ/).

Si toutefois le choix de la graphie ne permet pas de terminer l'alignement, et qu'il reste des phonèmes ou des graphèmes non alignés à la sortie de la boucle, ou que l'alignement d'un phonème est impossible sur le mot restant, l'algorithme revient en arrière pour prendre une graphie moins longue et retenter un alignement. Si le phonème précédent n'a pas d'autres graphies possibles, il remonte au phonème encore précédent. La configuration actuelle de l'algorithme permet de revenir jusqu'à 2 phonèmes en arrière, cela semble être suffisant.

Voici un exemple pour illustrer le retour en arrière de l'algorithme :
"essentiel" /esɑ̃sjɛl/
1. "essentiel" /esɑ̃sjɛl/ : l → 'l'
2. "essentie" /esɑ̃sjɛ/ : ɛ → 'e'
3. "essenti" /esɑ̃sj/ : j → 'i'
4. "essent" /esɑ̃s/ : s → 'ssent' (ou 'sent', 't')
5. "e" /esɑ̃/ : ɑ̃ → 'e'
6. "" /es/ : s → **alignement impossible** puisqu'il n'y a plus de lettre : on revient au phonème précédent
7. "e" /esɑ̃/ :  ɑ̃ → il n'y a pas d'autres graphies possibles que le 'e' ici : on revient au phonème d'encore avant
8. "essent" /esɑ̃s/ : il reste 'sent' et 't'. On prend la plus longue
9. "es" /esɑ̃/ : ɑ̃ → **alignement impossible** : on revient au phonème précédent
10. "essent" /esɑ̃s/ : s → il reste 't'. On le prend.
11. "essen" /esɑ̃/ : ɑ̃ → 'en'
12. "ess" /es/ : s → 'ss' (ou 's')
13. "e" /e/ : e → 'e'

Résultat : /e/→e, /s/→ss, /ɑ̃/→en, /s/→j, /ɛ/→e, /l/→l



*Phon2graph est écrit en Python version 3.7.1*
