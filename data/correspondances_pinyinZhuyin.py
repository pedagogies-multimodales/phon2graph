import json

py2zh = {}
with open('correspondances_big5GbHzZhuyinPinyinWadeYale.tsv', 'r') as inf:
    for line in inf:
        line = line.strip()
        l = line.split('\t')
        if len(l) == 7 and l[0]!="Big5":
            big5, gb, hz, zhuyin, pinyin, wade, yale = l
            py2zh[pinyin.lower().strip()] = {
                "Big5":big5.strip(),
                "GB":gb.strip(),
                "HZ":hz.strip(),
                "ZhuYin":zhuyin.strip(),
                "PinYin":pinyin.lower().strip(),
                "Wade":wade.lower().strip(),
                "Yale":yale.lower().strip()
            }

cpt=0
for i in py2zh.keys():
    cpt+=1
print(cpt,"entrées enregistrées.")

with open("pinyin2zhuyin.json","w") as outf:
    outf = json.dump(py2zh, outf, ensure_ascii=False, indent=4)

print('Export terminé.')

#### ATTENTION IL FAUT REMPLACER MANUELEMENT LES U: par Ü