


def pinyin2phon(pintone, pinyin2api, api2class):
    ## Prend en argument une chaîne type 'ni3' ou 'hao3'

    ##### LISTES EXCEPTIONS PINYIN #####
    xjq = ['x','j','q'] # suivis de -u → /-y/
    uun = ['u','un'] # précédés de xjq font /y/
    ilist = ['z','c','s','zh','ch','sh','r'] # -i is a buzzed continuation of the consonant following z-, c-, s-, zh-, ch-, sh- or r-.
    avoidlist = ['kw', 'wa', 'xw', 'ən', 'lj', 'nj', 'mj', 'jʊ', ] # éviter de les considérer comme un seul phonème


    car = pintone[:-1]
    ton = pintone[-1]

    if car in pinyin2api.keys():
        # Toute la graphie est dans pinyin2api # yi
        api = pinyin2api[car]
    else:
        if car[:2] in pinyin2api.keys():
            # 2 premiers caractères sont dans pinyin2api # zh eng
            api = pinyin2api[car[:2]] # api = zh
            if car[2:] in pinyin2api.keys(): # eng
                if car[:2] in ilist and car[2:] == 'i': # ! zh i / sh i
                    api += pinyin2api['_' + car[2:]]
                else:
                    api += pinyin2api[car[2:]] # zh eng
            else:
                api = ""
                print(car[:2], "trouvé ;", car[2:], "non trouvé!")
        
        elif car[:1] in pinyin2api.keys():
            # le premier caractère est dans pinyin2api # h ao
            api = pinyin2api[car[:1]]
            if car[1:] in pinyin2api.keys(): # ao
                if car[0] in xjq and car[1:] in uun: # ! qu / ju / xu
                    api += pinyin2api['_' + car[1:]]
                elif car[0] in ilist and car[1:] == 'i': # ! ri / ci / zi ...
                    api += pinyin2api['_' + car[1:]]
                else:
                    api += pinyin2api[car[1:]]
            else:
                api = ""
                print(car[:1], "trouvé ;", car[1:], "non trouvé!")
        else:
            print(car[:1], "non trouvé!")

    ## Conversion api2class
    phonlist = []
    
    phon = ''
    restapi = api
    while len(restapi) > 0:
        i=0
        trouve = True
        while trouve and i<=len(restapi):
            i+=1
            if restapi[:i] in api2class.keys() and restapi[:i] not in avoidlist:
                continue
                print(i, restapi[:i])
            else:
                i = i-1
                trouve = False
        print("trouvé",restapi[:i])
        phonlist.append(api2class[restapi[:i]])

        restapi = restapi[i:]
    
    return (car, api, phonlist, ton)
