# -*- encoding:utf8 -*- 

######### phon2graph.py #########
#
# Version Python 3.7.1
#
# ENGLISH PHON2GRAPH
#
# Prend en arguments :
#	- un mot
#	- sa transcription
#	- un dictionnaire qui donne chaque graphie pour chaque phonème
#	- un dictionnaire qui donne la classe de chaque phonème (classe css pour afficher en couleurs)
#   - un booléen qui active le mode verbose si True
#
#	Mode verbose : passer mode_verbose à True dans la fonction printlog(verb,)
#
# Sylvain Coulange (2020)
# cf. README pour explications détaillées


import re


def printlog(verb,*txt):
    if verb:
        outp = ''
        for i in txt:
            outp += str(i)+' '
        print(outp)


# DECOUPAGE PHONOGRAPHEMIQUE
def decoupage(mot,trans,phon2graph,phon2class,verb=False):
    phonographie = []

    # we take one phoneme after an other, except for these ones:
    bigr = ['aɪ','aʊ','əl','əm','ən','əʊ','dz','dʒ','ef','eɪ','eə','ɪə','ɡz','ɡʒ','ks','kʃ','kw','ɔɪ','ts','tʃ','ʊə','wʌ','ju','jʊ','iz','ɪz','əz']
    trigr = ['jʊə','juː','wɑː','waɪ']
    
    # let's get rid of other special characters
    trans = trans.replace('.','')
    trans = trans.replace('‿','')
    trans = trans.replace(' ','')
    trans = trans.replace('(','')
    trans = trans.replace(')','')
    trans = trans.replace('͡','')
    trans = trans.replace('*','')

    trans = trans.replace('ˈ','')
    trans = trans.replace('ˌ','')
    
    #mot = mot.replace('-','')
    #mot = mot.replace(' ','')

    # to be sure to treat lower word
    mot = mot.lower()

    # Déclarations préalables des mémoires 1 et 2
    graphWinners_precedent2 = []
    graphWinners_precedent = []
    mot_save2 = ''
    mot_save = ''
    phon_save2= ''
    phon_save = ''
    trans_save2 = ''
    trans_save = ''

    echec = False # permet de renvoyer le mot sans trancription si impossible à coloriser (ex CM2)
    motOrigin = mot # permet de renvoyer le mot d'origine

    # Association phonème→graphème
    # Boucle sur la chaîne de phonèmes à partir du dernier
    while trans != '':
        printlog(verb,'##################################')
        printlog(verb,'Départ : "',mot,'", [',trans,']')
        
        if len(mot) == 0:
            echec = True
            printlog(verb,'ÉCHEC: plus de lettre à aligner!')
            break
        if len(trans) == 0:
            echec = True
            printlog(verb,'ÉCHEC: plus de phonème à aligner!')
            break

        if not echec:
            if mot[-1] == ' ' or mot[-1] == '-':
                printlog(verb,'Dernier caractère de mot est une espace ou un tiret: on ajoute ce caractère en phon_neutre')
                phonographie.append(("phon_neutre",mot[-1]))
                mot = mot[:-1]
            elif trans[-1] == '‿':
                printlog(verb,'Dernier caractère de transcription est "‿": on ajoute ce caractère à mot avec aligné au phonème qui suit.')
                phonographie.append((phon2class[trans[-2]],'‿'))
                trans = trans[:-2] # on supprime les deux derniers car de la transcription x‿
            else:
                phon = ''
                if trans[-3:] in trigr:
                    if re.match(r'^.*(ure|u|eu|ue|ew|ui|ewe|ut|eue|eau|oir|oi|oire|ois)$',mot): # consider 3 last phonemes as "one" only if appropriate spelling is found
                        phon = trans[-3:]
                    else:
                        phon = trans[-2:] # else consider only the 2 last phonemes

                if trans[-2:] in bigr and len(phon)<3:
                    if re.match(r'^.*('+'|'.join(phon2graph[phon2class[trans[-2:]]])+')$',mot):
                        phon = trans[-2:]
                    else:
                        phon = trans[-1]
                    
                # Else take only the last caracter of transcription as The Phoneme to align
                else:
                    phon = trans[-1]

                printlog(verb,"Search : [",phon,"]")

                # Get all potential spelling given our word
                graphWinners = [] # list of possible spelling (longer first)
                if phon2class[phon] in phon2graph.keys():
                    for graph in phon2graph[phon2class[phon]]:
                        # Can we find this spelling at the end of our word ?
                        s = re.match(r'^.*'+graph+'$',mot)
                        if s :
                            graphWinners.append(graph)
                            printlog(verb,'found',graph)
                    if phon in ['ə','ʊ','u','i','ɪ']:
                        if phon == 'ə': schw = 'phon_schwa'
                        if phon in ['ʊ','u']: schw = 'phon_schwu'
                        if phon in ['i','ɪ']: schw = 'phon_schwi'

                        for graph in phon2graph[schw]:
                            # Can we find this spelling at the end of our word ?
                            s = re.match(r'^.*'+graph+'$',mot)
                            if s :
                                graphWinners.append(graph)
                                printlog(verb,'found',graph)

                else:
                    printlog(verb,'Phoneme',phon,phon2class[phon],'not found in the Fidel!')
                
                graphWinners = sorted(graphWinners, key=len) # longer spelling first
                printlog(verb,graphWinners)

                if len(graphWinners)>0:
                    printlog(verb,'OK len(graphWinners)>0',graphWinners, 'ajout de :', graphWinners[-1])
                    phonographie.append((phon2class[phon],graphWinners[-1]))
                    
                    # MISES EN MÉMOIRE (1→2, courant→1)
                    graphWinners_precedent2 = graphWinners_precedent # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog(verb,'update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog(verb,'update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save
                    printlog(verb,'update phon_save2 :',phon_save2)
                    phon_save = phon # on garde en mémoire le phonème aussi
                    printlog(verb,'update phon_save :',phon_save)
                    
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription
                    
                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(graphWinners[-1])]
                    trans = trans[:-len(phon)] # et on enlève le dernier phonème pour passer au suivant
                    printlog(verb,'trans :',trans_save,'→',trans)

                    printlog(verb,'phonographie :',phonographie)

                elif len(graphWinners_precedent) > 0:
                    # Cas où on a pas trouvé de graphie pour le phonème courant, parce que la graphie précédente n'était pas la bonne (elle est trop longue)
                    remplacant = graphWinners_precedent[-1]
                    printlog(verb,"Aucune graphie touvée ! → On revient sur le dernier phonème et on prend sa graphie suivante :", remplacant)
                    phonographie = phonographie[:-1] # on supprime la dernière graphie (qui n'était pas la bonne)
                    phonographie.append((phon2class[phon_save],remplacant)) # on y met à la place le graphie suivante avec son phonème
                    
                    # CORRECTION DES VARIABLES
                    trans = trans_save # on remet la transcription du coup d'avant (avant de la tronquer plus bas)
                    mot = mot_save # pareil avec le mot

                    # MISES EN MÉMOIRE (1→2, courant→1)
                    #graphWinners_precedent2 = graphWinners_precedent[:-1] # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog(verb,'update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners_precedent[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog(verb,'update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save2
                    printlog(verb,'update phon_save2 :',phon_save2)
                    phon_save = phon_save # on garde en mémoire le phonème aussi
                    printlog(verb,'update phon_save :',phon_save)
                    
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription
                    
                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(remplacant)]
                    trans = trans[:-len(phon_save)] # et on enlève le dernier phonème pour passer au suivant (attention phon_save est passé à save2)
                    printlog(verb,'trans :',trans_save,'→',trans)

                    printlog(verb,'phonographie :',phonographie)

                elif len(graphWinners_precedent2) > 0:
                    # Si pas d'autres graphies possibles pour le phonème précédent, on va chercher la graphie suivante du phonème d'avant
                    remplacant = graphWinners_precedent2[-1]
                    phonConcerne = phon_save2
                    printlog(verb,"Aucune graphie touvée ! Ni pour le phonème d'avant ! → on va chercher la graphie suivante du phonème encore avant :",remplacant)
                    phonographie = phonographie[:-2] # on supprime les 2 dernières graphies (qui n'étaient pas la bonnes)
                    phonographie.append((phon2class[phonConcerne],remplacant)) # on y met à la place le graphie suivante avec son phonème
                    
                    # CORRECTION DES VARIABLES
                    trans = trans_save2 # on remet la transcription du coup d'avant (avant de la tronquer plus bas)
                    mot = mot_save2 # pareil avec le mot

                    # MISES EN MÉMOIRE (1→2, courant→1)
                    #graphWinners_precedent2 = graphWinners_precedent2[:-1] # on fait passer la mémoire des autres graphies du phonème précédent dans la seconde mémoire avant d'écraser la première avec la liste des graphies du phonème courant (cas de essentiellement, on doit revenir 2 phonèmes en arrières)
                    printlog(verb,'update graphWinners_precedent2 :',graphWinners_precedent2)
                    graphWinners_precedent = graphWinners_precedent2[:-1] # on garde en mémoire la liste des autres graphies possibles pour y revenir si besoin
                    printlog(verb,'update graphWinners_precedent :',graphWinners_precedent)
                    
                    phon_save2 = phon_save
                    printlog(verb,'update phon_save2 :',phon_save2)
                    phon_save = phon # on garde en mémoire le phonème aussi
                    printlog(verb,'update phon_save :',phon_save)
                
                    mot_save2 = mot_save # on passe mot_save dans la deuxième mémoire
                    mot_save = mot # on passe mot en mémoire avant de lui enlever les lettres de la graphies choisie
                    
                    trans_save2 = trans_save # on passe trans_save dans la deuxième mémoire
                    trans_save = trans # on garde en mémoire l'état actuel de la transcription

                    # MISE À JOUR DE mot ET trans
                    mot = mot[:-len(remplacant)]
                    trans = trans[:-len(phonConcerne)] # et on enlève le dernier phonème pour passer au suivant (attention phon_save2 a été écrasé donc il faut le garder en mémoire temporaire (phonConcerne))
                    

                    printlog(verb,'trans :',trans_save,'→',trans)

                    printlog(verb,'phonographie :',phonographie)
                else:
                    printlog(verb,'AUCUNE GRAPHIE TROUVÉE POUR CE MOT !')
                    echec = True
                    break
    ## FIN DE LA BOUCLE SUR TRANSCRIPTION

    ## FINALISATION ET ENVOI DU RÉSULTAT
    if echec:
        printlog(verb,"Mot impossible à coloriser. Renvoie du mot sans couleur.")
        phonographie = [('phon_echec',motOrigin)]
        printlog(verb,("Phonographie :",phonographie))
    else:
        # Si le mot a bien été colorisé jusqu'à la fin
        # On remet tout dans l'ordre et on envoie
        phonographie.reverse()
        printlog(verb,"FINI !",phonographie)
        
    printlog(verb,"###########")
    printlog(verb,"###########")
    return phonographie # revoie donc un liste de (phonème(s), graphème(s))