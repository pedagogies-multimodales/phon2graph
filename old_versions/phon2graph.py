# -*- encoding:utf8 -*- 

######### phon2graph.py #########
#
# Version Python 3.7.1
#
# Prend en entrée :
#	- dictionnaire phonème→couleur (phoneme-couleur.csv)
#	- dictionnaire phonème→graphies (phoneme-graphies_fr.scsv)
#	- dictionnaire phonétisé (dico_frwiki.csv)
#
#	Mode verbose : -v (à faire)
#
# cf. README pour explications détaillées

import re

# LECTURE DU CODE PHONEME-COULEUR
phonFile = open("data/phoneme-couleur.csv",mode="r")
phon2col = {}
phonCpt = 0

for line in phonFile:
	line = line.strip()
	l= line.split(',')

	if len(l) == 3:
		phon,col,phonId = l
		phonCpt += 1
		phon2col[phon]=[]
		phon2col[phon].append(col)
		phon2col[phon].append(phonId)

print(phonCpt,"codes phonème-couleur détectés.")
phonFile.close()


# LECTURE DE LA LISTE PHONEME-GRAPHIES (FIDEL)
phonFile = open("data/phoneme-graphies_fr.scsv",mode="r")
phon2graph = {}
phonCpt = 0
graphCpt = 0

for line in phonFile:
	phonCpt+=1
	line = line.strip()
	l= line.split(':')

	phon2graph[l[0]] = []

	listegraphies = l[1].split(',')
	for graph in listegraphies:
		phon2graph[l[0]].append(graph)
		graphCpt+=1

	#print(l[0],phon2graph[l[0]])

print(phonCpt,"phonèmes pour le français.")
print(graphCpt,"graphies extraites du Fidel français.")
phonFile.close()



# LECTURE DU DICTIONNAIRE
dicoFile = open("data/dico_frwiki.csv",mode="r")
word2trans = {}
wordCpt = 0

for line in dicoFile:
	line = line.strip()
	l = line.split(',')

	if len(l) == 3:
		num,mot,trans = l
		wordCpt += 1
		word2trans[mot] = trans

print(wordCpt,"mots enregistrés.")
dicoFile.close()


# DECOUPAGE PHONOGRAPHEMIQUE
def decoupage(mot,trans):
    phonographie = []

    # Listes des exceptionss
    scphListe = ['s','c','p'] # avec -h donnent 1 phonème
    xListe = ['ts','tʃ','dz','dʒ','ks','ɡz','wa','wɛ̃','ɥi'] # donnent 1 graphème
    
    # Prétraitements de la transcription et du mot orthographié
    trans = trans.replace('.','')
    trans = trans.replace('‿','')
    trans = trans.replace(' ','')
    trans = trans.replace('(','')
    trans = trans.replace(')','')
    trans = trans.replace('͡','')
    mot = mot.lower()
    mot = mot.replace('-','')
    mot = mot.replace(' ','')

    # Association phonème→graphème
    # Boucle sur la chaîne de phonèmes à partir du dernier
    while trans != '':
        #print(mot,trans) # décommenter pour afficher le détails dans le terminal
        
        # Traitement des caractères phonèmes à prendre par deux
        if trans[-1] == '̃' or trans[-2:] in xListe:
            if trans[-2:]=='ks' and not re.match(r'.*x.*',mot): # éviter de traiter 'ct' (fonction) comme 'x'
                phon = trans[-1]
            elif trans[-2:]=='dʒ' and re.match(r'.*dj.*',mot): # si 'dj' (adjectif) alors 'j' = [ʒ] et pas [dʒ]
                phon = trans[-1]
            elif trans[-2:]=='ɥi' and not mot[-1]=='u':
                phon = 'i'
            else:
                phon = trans[-2:]
                trans = trans[:-1]

        # Cas du wɛ̃ (3 caractères phonèmes)
        elif trans[-3:] in xListe:
            phon = trans[-3:]
            trans = trans[:-2]
        else:
            phon = trans[-1]

        # Récupération de la graphie associée dans le Fidel
        graphWinner = ''
        if phon in phon2graph.keys():
            # Boucle sur la liste de graphies possible pour le phonème phon
            for graph in phon2graph[phon]:
                # On cherche cette graphie dans mot
                s = re.match(r'.*'+graph+'$',mot)
                if s and graph[0]=='h' and len(mot)>len(graph) and mot[-(len(graph)+1)] in scphListe: # cas de 'ch-' 'sh-' 'ph-'
                    graphWinner = graph[1:]
                elif s and len(graph)>len(graphWinner):
                    #print('trouvé',graph)
                    graphWinner = graph
        else:
            print('Phonème',phon,'non trouvé dans le Fidel !')
            
        phonographie.append((phon2class[phon],graphWinner))
        mot = mot[:-len(graphWinner)]
        trans = trans[:-1]

    # On remet tout dans l'ordre et on envoie
    phonographie.reverse()
    return phonographie # revoie donc un liste de (phonème(s), graphème(s))


# TRANSCRIPTION EN COULEUR
def printCouleur(exphon):
	if exphon in phon2col.keys():
		print(exphon,phon2col[exphon][0])
	else:
		print(exphon)

def getCouleurs(trans):
	exphon=''
	for phon in trans:
		if phon == '̃':
			exphon = exphon+phon
		else:
			printCouleur(exphon)
			exphon = phon
	printCouleur(exphon)


# INTERFACE
quit = False
while not quit:
	print("Tapez un mot à découper (ou $ pour quitter)...")
	mot = input()

	if mot == "$":
		quit=True
		break

	if mot in word2trans.keys():
		trans = word2trans[mot]
		print('Transcription :',trans)
		print('Découpage phonographémique :',decoupage(mot,trans))
		getCouleurs(trans)
	else:
		print("Désolé, ce mot n'est pas dans le dictionnaire.")

'''
# BOUCLE SUR DICO
fOut = open('dico_frwiki_decoupage.csv',mode='w')
num=1
for i,j in word2trans.items():
	decoup = decoupage(i,j)
	print(i,' - ',j,' - ',decoup)
	fOut.write('{},{},{},{}\n'.format(num,i,j,decoup))
	num+=1
fOut.close()
'''

# problèmes restants : couvent/couvent fils/fils est/est → analyse syntaxique avec Spacy ; → problème dans dico : entrée graphique unique
# problèmes restants : CD-ROM, T-shirt, SNCF... yaourt(aou pris pour ou)
# millet (graphWinner=ill). Il faut pouvoir vérifier si consonne précède le i
# opter → pt considéré comme /t/

# transcriptions manquantes ou incomplètes ou incorrectes : moins, badge, oignon, hiboux

# pour réussir tuyau, Guyane : j'ai supprimé 'ij' de la xListe

# faudrait ajouter une vérification: si reste graphèmes mais qu'on a utilisé tous les phonèmes, recommencer avec une contrainte : ça permettrait d'éviter des erreurs sur opter, yaourt...

# Ajouter les bicolors !!
# Refaire le dictionnaire avec multientrée fils/fils


# REMARQUES:
# Pistes : Apprentissage automatique des patterns de transcription →quel algo?
# Pistes : modèle statistique (dans tel contexte graphique, quel pourcentage pour chaque graphie du phonème?) →ngram
# Pistes : grammaire de règles prenant en compte la position de la graphie dans le mot. [ɛ] ne s'écrira "aid" qu'à la fin d'un mot.
# Débat sur forum : https://openclassrooms.com/forum/sujet/comment-decoupe-une-chaine-de-caractere-en-syllabe-23388
