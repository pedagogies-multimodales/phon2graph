# -*- encoding:utf8 -*- 

######### phon2graph_english.py #########
#
# Version Python 3.7.1
#
# ENGLISH PHON2GRAPH
#
# Prend en entrée :
#	- dictionnaire phonème→graphies (Fidel)
#	- dictionnaire phonétisé
#
# Sylvain Coulange (2020)
# cf. README pour explications détaillées

import re
import json
from phon2graph_english import decoupageEn


############ DÉCLARATIONS DES CHEMINS ############
classPhonFilePath = "data/api2class.json"
fidelFilePath = "data/fidel_wikicolor_en.scsv"
dicoFilePath = "../wikiphon/dico_enWiktionary-20200704_v1.json"
##################################################

# LECTURE DU CODE PHONEME-COULEUR
with open(classPhonFilePath,"r") as phonFile:
    phon2class = json.load(phonFile)

# LECTURE DU DICTIONNAIRE PHONEME-GRAPHIES (FIDEL)
fidel = open(fidelFilePath,mode="r")
phon2graph = {}
phonCpt = 0
graphCpt = 0

for line in fidel:
	phonCpt+=1
	line = line.strip()
	l= line.split(':')

	phon2graph[l[0]] = []

	listegraphies = l[1].split(',')
	for graph in listegraphies:
		phon2graph[l[0]].append(graph)
		graphCpt+=1

print(phonCpt,"phonemes detected.")
print(graphCpt,"spellings extracted from the Fidel.")
fidel.close()

# LECTURE DU DICTIONNAIRE PHONÉTISÉ
wordCpt = 0

with open(dicoFilePath) as json_data:
    dico = json.load(json_data)

for i in dico.keys():
	wordCpt += 1

print(wordCpt,"entries in the dictionary file.")


# INTERFACE
quit = False
while not quit:
	print("Insert a word to phonetize and align (or $ to quit)...")
	word = input()
	word = word.lower()

	result = []

	if word == "$":
		quit = True
		break

	if word[-1] == "?":
		verb = True
		word = word[:-1]
	else:
		verb = False

	for w,locs in dico.items():
		if w.lower() == word:
			result.append((w,locs))
	
		
	print(len(result),'result(s) found!')
	
	for res in result:
		print("« " + res[0] + " »")
		for loc, transs in res[1].items():
			
			for trans in transs:
				print("\n\t["+loc+"] " + trans + " ")
				if trans[0] == "/":
					print(decoupageEn(res[0],trans.replace('/',''),phon2graph,phon2class,verb))
		
		

		

dico = []