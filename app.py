# -*- encoding:utf8 -*- 

######### phon2graph.py #########
#
# Version Python 3.7.1
#
# Prend en entrée :
#	- dictionnaire phonème→couleur (phoneme-couleur.csv)
#	- dictionnaire phonème→graphies (Fidel) (phoneme-graphies_fr.scsv)
#	- dictionnaire phonétisé (dico_frwiki.csv)
#
# Sylvain Coulange (2020)
# cf. README pour explications détaillées

import re, json
from phon2graph_french import decoupage


############ DÉCLARATIONS DES CHEMINS ############
phonColFile = "data/class-phoneme_v2.csv"
fidelFilePath = "data/phoneme-graphies_fr.scsv"
dicoFilePath = "../wikiphon/dico_frwiktionary-20200301_v2.json"
##################################################

# LECTURE DU DICTIONNAIRE PHONEME-CLASSE
phonFile = open(phonColFile,mode="r")
phon2class = {}
phonCpt = 0
for line in phonFile:
	line = line.strip()
	l= line.split(',')

	if len(l) == 2:
		phonId,phon = l
		phon2class[phon]=phonId
		phonCpt += 1

print(phonCpt,"classes-phonèmes détectées.")
phonFile.close()

# LECTURE DU DICTIONNAIRE PHONEME-GRAPHIES (FIDEL)
fidel = open(fidelFilePath,mode="r")
phon2graph = {}
phonCpt = 0
graphCpt = 0

for line in fidel:
	phonCpt+=1
	line = line.strip()
	l= line.split(':')

	phon2graph[l[0]] = []

	listegraphies = l[1].split(',')
	for graph in listegraphies:
		phon2graph[l[0]].append(graph)
		graphCpt+=1

print(phonCpt,"phonèmes pour le français.")
print(graphCpt,"graphies extraites du Fidel français.")
fidel.close()

# LECTURE DU DICTIONNAIRE PHONÉTISÉ
print("Chargement de",dicoFilePath,"...")
word2trans = {}
with open(dicoFilePath,'r') as dic:
    word2trans = json.load(dic)

print("OK.")
print(len(word2trans.keys()),"mots enregistrés dans le dictionnaire phonétisé.")


# INTERFACE
quit = False
while not quit:
    print("Tapez un mot à découper (ou $ pour quitter)...")
    mot = input()
    resbol = False
    
    if mot == "$":
        quit = True
        break
    
    for w,tt in word2trans.items():
        if mot == w:
            resbol = True
            print("Trouvé :",w,tt)
            for t in tt:
                print(decoupage(w,t,phon2graph,phon2class,verb=True))
    
    if resbol:
        print("Mot non trouvé dans le dictionnaire.")

print("À bientôt !")
word2trans = {}

# Débat sur forum : https://openclassrooms.com/forum/sujet/comment-decoupe-une-chaine-de-caractere-en-syllabe-23388
